# Campaign Site

## Contribution

If you've come to help build this site, thanks! Please follow these guidelines:


- Use any website languages/frameworks that you like (I would like to host via Gitlab pages)
- Include the following pages {Home, About, Stances, Donate, Contact}
- Include a summary text file with instructions on setting up a dev environment in order for me to see your work
- Use image placeholders of whatever graphics you feel are appropriate (I don't have any to provide yet)
- Lorem Ipsum all of the page's for now. Things like the actual donation mechanics can come later

I won't add anyone to this group just yet so in the mean time please fork this project, create a branch with your name/handle, and submit pull requests to send in your ideas.

Feel free to ask questions to clarify -- I've never asked for this kind of help before so I'm likely not forming the task well. But we can get there together :)

This website is a critical component needed to get my campaign rolling for real and I appreciate any and all who pitch in to make it happen!

TC
